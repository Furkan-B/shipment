package com.example.shipment.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShipmentPackage {
    private int height;
    private int width;
    private int length;
    private int weight;

    public ShipmentPackage() {
    }

    public ShipmentPackage(int height, int width, int length, int weight) {
        this.height = height;
        this.width = width;
        this.length = length;
        this.weight = weight;
    }
}
