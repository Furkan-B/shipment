package com.example.shipment.exception;

public class ShipmentNotCalculatedException extends Exception {
    public ShipmentNotCalculatedException(String message) {
        super(message);
    }
}
