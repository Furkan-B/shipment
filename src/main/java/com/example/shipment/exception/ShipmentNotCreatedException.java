package com.example.shipment.exception;

public class ShipmentNotCreatedException extends Exception {
    public ShipmentNotCreatedException(String message) {
        super(message);
    }
}
