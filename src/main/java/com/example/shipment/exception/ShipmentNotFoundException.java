package com.example.shipment.exception;

public class ShipmentNotFoundException extends Exception {
    public ShipmentNotFoundException(String message){
        super(message);
    }
}
